This repository was built for my 'Data' project as part of my course.

The main objective was to create a GUI interface where data could be read in
and interpreted in whatever way I saw fit.

This project allowed me to have a full GUI interface, where I learned about
the various data structures available to Java. Through my extensive research
I was able to optimise the system to be able to read 400,000 data records,
rather than just the required 1,000. This was something I found a challenge
during this project, as my initial algorithms did not allow for sorting huge 
amounts of data, but they would sort a small amount. This is due to the 
exponential nature of the algorithms, and the commits within should show the
changes in algorithms that proves my ability to problem solve 
and optimize in this manner.