import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 **/

/***
 *
 * @author Matt
 **/
public class graphComponent {     
    private ChartPanel chartPanel;
    private dataHandler _data;
    private String _address;

    public graphComponent(String chartTitle, dataHandler data, String address) {
        DefaultCategoryDataset dataset = data.createDataset(address);
        JFreeChart chart = ChartFactory.createLineChart("Sensor Data", "Date", "Sensor Reading", dataset);
        chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1200, 800));
        _data = data;
        _address = address;
    }
    
    public ChartPanel getContentPane()
    {
        return chartPanel;
    }
    

    /***
     * Creates a chart
     **/
}
