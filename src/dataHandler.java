import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import org.jfree.data.category.DefaultCategoryDataset;


/***
 * This class handles all the data, including reading
 * the file and extracting data, sorting data, etc.
 * 
 * @author Matt
 **/

public class dataHandler {
    
    private String readLine = "";      //Used for getting the line to be split from the csv file
                                       
    private File _mainFile;     //The CSV file transferred from mainmenu.
    
    private final String csvValue = ",";  //Defines what splits the csv file (space, "", comma)
    
    private int rowsLoaded = 0;         //Counter to show how many rows are loaded from the file.
    
    private int noTransmitters = 0;     //To count the number of transmitters.
    
    private String Transmitters[] = new String[100];     //Used for the main menu to display transmitters for selection
    
    private ArrayList<dataObject> dataStorage = new ArrayList<dataObject>();  //Holds all of the data from the CSV file
    
    private boolean found = false;
    
    private int i = 0;  //Used as an index.
    
    private int noFaults = 0;
    
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss"); //The date format that is printed on the main menu.

     private SimpleDateFormat graphFormat = new SimpleDateFormat("dd-MM-yyyy hh"); //The date format that is printed on graphs.
    /**
     * This constructer takes the file from the main menu
     * and assigns it locally for use in the dataHandler.
    **/
    public dataHandler(File mainFile)
    {
        _mainFile = mainFile;
    }
    
    /**
        Loads the file based on the one loaded in the constructor.
        Also determines the number of transmitters and stores 
        each of them in an array.
        Stores each of the rows in the csv file in the ArrayList.
    **/
    public void loadFile()
    {
        try (BufferedReader fileRead = new BufferedReader(new FileReader(_mainFile))) 
        {
            while((readLine = fileRead.readLine()) != null)
            {
                String[] row = readLine.split(csvValue);
                
                rowsLoaded++;
                      
                dataObject newLine = new dataObject(Integer.parseInt(row[0]), row[1], row[2], row[3], row[4], row[5], row[6], row[7]);
                
                checkNewTransmitter(row, newLine);
                
                dataStorage.add(newLine);
            }
            Collections.sort(dataStorage);
        } 
        catch (IOException e)
        {
                e.printStackTrace();
        }
    }

    /**
    Build the table model for the errorFrame table.
    Checks if filter applies.
    **/
    public void buildTable(JTable errorsTable, String filter)
    {
        DefaultTableModel model = new DefaultTableModel();
        errorsTable.setModel(model);
        
        model.setRowCount(0);
        model.addColumn("Address");
        model.addColumn("Via");
        model.addColumn("Status");
        model.addColumn("Type");
        model.addColumn("Version");
        
        if(filter.equals("All"))
        {
            for(int i = 0; i < dataStorage.size(); i++)
            {
                if((dataStorage.get(i).isBroken() == true))
                {
                    model.addRow(new Object[]{String.valueOf(dataStorage.get(i).getAddress()), String.valueOf(dataStorage.get(i).getVia()), String.valueOf(dataStorage.get(i).getStatus()), String.valueOf(dataStorage.get(i).getType()), String.valueOf(dataStorage.get(i).getVersion())});
                }
            }
        }
        else
        {
            for(int i = 0; i < dataStorage.size(); i++)
            {
                if((dataStorage.get(i).isBroken() == true) && dataStorage.get(i).getAddress().equals(filter))
                {
                    model.addRow(new Object[]{String.valueOf(dataStorage.get(i).getAddress()), String.valueOf(dataStorage.get(i).getVia()), String.valueOf(dataStorage.get(i).getStatus()), String.valueOf(dataStorage.get(i).getType()), String.valueOf(dataStorage.get(i).getVersion())});
                }
            }
        }
        
    }
    
    /**
    Method used to check if a new transmitter is in the row.
    If it is new, then adds it to the Transmitters array.
    **/
    public void checkNewTransmitter(String[] row, dataObject newLine)
    {
        i = 0;
                
        found = false;
        
        if(newLine.isBroken())
        {
            noFaults++;
        }
                
        while(!found)
        {
            if(Transmitters[i] == null)
            {
                Transmitters[i] = row[5];
                noTransmitters++;
                found = true;
 
            }
            else if(Transmitters[i].equals(row[5]))
            {
                found = true;
            }
            i++;
        }
    }
    
    /**
    Takes the data of 'Transmitters' array and uses it to populate
    the dropdown box on the main page.
    **/
    public void populateDropdown(JComboBox sensorSelect)
    {
        found = false;
        i = 0;
        sensorSelect.removeAllItems();
        while(!found)
        {
            if(Transmitters[i] == null)
            {
                found = true;
            }
            else
            {
                sensorSelect.addItem(Transmitters[i]);
            }
            i++;
        }
        
    }
    
    
    /**
    For creating the graph dataset.
    **/
    public DefaultCategoryDataset createDataset(String address) {
                
        String _address = address;
        
        // XYSeries[] sensor = new XYSeries[100];
        
        String[] sensor = new String[10];
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        int sensorIndex = 0;
        
        String date = "";

        for(int j = 0; j < 10; j++)
        {
            sensor[j] = ("Sensor " + (j+1));
        }

        for(int i = dataStorage.size()-1; i >= 0; i--)
        {
            if((dataStorage.get(i).getAddress()).equals(_address))
            {
                sensorIndex++;
                for(int x = 0; x < 10; x++)
                {
                    date = dateFormat.format(dataStorage.get(i).getDate().getTime());
                    dataset.addValue(dataStorage.get(i).getArrayData(x), String.valueOf(sensor[x]), date);
                }
            }
        }
        return dataset;

    }
    
    /**
    Used to find the transmitters most recent time recorded
    **/
    public String findTransmitter(String transmitterID)
    {
        String date = "";
        for(int x = 0; x < dataStorage.size(); x++)
        {
            if((dataStorage.get(x).getAddress()).equals(transmitterID))
            {   
                date = dateFormat.format(dataStorage.get(x).getDate().getTime());
                
                return date;
            }
            x++;
        }
        return "";
    }
    
    /**
    Passes the labels and uses the information to update them.
    **/
    public void updateInformation(String transmitterID, JLabel receiver, JLabel system, JLabel status, JLabel device, JLabel latest)
    {   
        String newDate = dateFormat.format(dataStorage.get(0).getDate().getTime());
        
        latest.setText(newDate);
        
        for(int x = 0; x < dataStorage.size(); x++)
        {
            if((dataStorage.get(x).getAddress()).equals(transmitterID))
            {
                receiver.setText(dataStorage.get(x).getVia());
                system.setText(dataStorage.get(x).getVersion());
                status.setText(dataStorage.get(x).getStatus());
                device.setText(dataStorage.get(x).getType());
            }
            x++;
        }
    }
    
    /**
    Updates the sensor box with the latest sensor reading, the mean, the range and min/max values.
    **/
    public void sensorBoxUpdate(JComboBox sensorCBox, String transmitterID, int sensorNumber, JLabel reading, JLabel meanReading, JLabel rangeReading, JLabel maxValue, JLabel minValue, JButton graph)
    {
        float mean = 0;
        int currentValue = 0;
        int noValues = 0;
        
        int min = 300;
        int max = 0;
        int range = 0;
        
        boolean broken = false;
        
        for(int x = 0; x < dataStorage.size(); x++)
        {
            if((dataStorage.get(x).getAddress()).equals(transmitterID) && (dataStorage.get(x).isBroken()) == true)
            {
                broken = true;
                sensorCBox.setEnabled(false);
                graph.setEnabled(false);
                JOptionPane.showMessageDialog(null, "This transmitter has bad sensor data. Consult errors for more information.", "Error", JOptionPane.ERROR_MESSAGE);
                break;
            }
            else
            {
                sensorCBox.setEnabled(true);
                graph.setEnabled(true);
            }
            x++;
        } 
        
        if(!broken)
        {
            for(int x = 0; x < dataStorage.size(); x++)
            {
                if((dataStorage.get(x).getAddress()).equals(transmitterID))
                {
                    reading.setText(String.valueOf(dataStorage.get(x).getArrayData(sensorNumber-1)));
                }
                x++;
            } 
        
            for(int x = 0; x < dataStorage.size(); x++)
            {
                if((dataStorage.get(x).getAddress()).equals(transmitterID))
                {
                    currentValue = dataStorage.get(x).getArrayData(sensorNumber-1);
                
                    mean+= currentValue;
                    noValues++;
                
                    if(currentValue < min)
                    {
                        min = currentValue;
                    }
                    if(currentValue > max)
                    {
                        max = currentValue;
                    }
                
                }
           
            }
        
            range = max - min;

            mean /= noValues;
            meanReading.setText(String.valueOf(mean));
            maxValue.setText(String.valueOf(max));
            minValue.setText(String.valueOf(min));
            rangeReading.setText(String.valueOf(range));
        
        } 
    }
    
    /**
     * Returns the number of rows the program has loaded
     * based on the results of loadfile.
    **/
    public int getRowsLoaded()
    {
        return rowsLoaded;
    }
    
    /**
    Used to send the number of transmitters to main menu
    for display on the form.
    **/
    public int getSensorsLoaded()
    {
        return noTransmitters;
    }
    
    
    /**
    Get the number of faulty rows picked up
    **/
    public int getFaults()
    {
       return noFaults;
    }
    
    
}
