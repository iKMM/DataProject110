import java.util.Calendar;

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 **/

/***
 * The data object class should hold the data as 'rows' that comes
 * from the csv file.
 * 
 * Also holds a sensor data array, which will contain the sensor data
 * split up into 10 different fields. Array is size 20 for redundancy.
 *
 * @author Matt
 **/
public class dataObject implements Comparable<dataObject> {
    
    private Calendar timeOnClock = Calendar.getInstance();
    
    private int _seconds = 0;
    private String _type = "err";
    private String _version = "err";
    private String _counter = "err";
    private String _via = "err";
    private String _address = "err";
    private String _status = "err";
    private String _sensorData = "err";
    
    private boolean _sensorBroken = false;
    
    private int[] sensorDataArray = new int[20];
    
    /**
     * The constructor for this class, takes in a line of the csv file and 
     * stores each variable.
     * Also converts the seconds to an actual calendar time, which is stored.
     *
    **/
    public dataObject(int seconds, String type, String version, String counter, String via, String address, String status, String sensorData)
    {          
        timeOnClock.set(2000, Calendar.JANUARY, 01, 00, 00, 00);
            
        timeOnClock.add(Calendar.SECOND, seconds);
            
        _seconds = seconds;
        _type = type;
        _version = version;
        _counter = counter;
        _via = via;
        _address = address;
        _status = status;
        _sensorData = sensorData;
        
        _sensorBroken = checkBroken(_status);
        
        setSensorArray();
    }
    
    /**
    Checks if status code is anything but 0.
    **/
    private boolean checkBroken(String _status)
    {
        return (Integer.parseInt(_status, 16) != 0);
    }
    
    @Override
    public int compareTo(dataObject comparison)
    {
        int compare = ((dataObject)comparison).getSeconds();
        
        //return this._seconds-compare;
        
        return compare - this._seconds;
    }
    
    /**
    Used for cutting up the sensor data into 10 8 bit chunks.
    **/
    public void setSensorArray()
    {
        String newData;
        int k = 0;
        
        if(!_sensorBroken)
        {
            for(int i = 0; i < 10; i++)
            {
            newData = _sensorData.substring(k, k+2);
            
            sensorDataArray[i] = Integer.parseInt(newData, 16);
            
            k+=2;
            
            }
        }
        
        
    }
    
    public Calendar getDate()
    {
        return timeOnClock;
    }
    
    public int getSeconds()
    {
        return _seconds;
    }
    
    public String getType()
    {
        return _type;
    }
    
    public String getVersion()
    {
        return _version;
    }
    
    public String getCounter()
    {
        return _counter;
    }
    
    public String getVia()
    {
        return _via;
    }
    
    public String getAddress()
    {
        return _address;
    }
    
    public String getStatus()
    {
        return _status;
    }
    
    public String getSensorData()
    {
        return _sensorData;
    }
    
    public int getArrayData(int i)
    {
        return sensorDataArray[i];
    }
    
    public boolean isBroken()
    {
        return _sensorBroken;
    }

}
