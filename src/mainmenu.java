import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;

/*** 
 * Mainmenu class.
 * 
 * Used to build the main interface seen in the program.
 * Contains all the variable declarations for the panels etc.
 *
 * @author Matt
 **/
public class mainmenu extends javax.swing.JFrame {

    /***
     * Creates new form 'mainmenu'.
     * Calls method initcomponents to build the form.
     **/
    
    dataHandler data; //The main dataHandler class.
    
    public mainmenu() {
        initComponents();
    }
    
    private String transmitter;

    /***
     * This method is called from within the constructor to initialize the form.
     * The code contains all the setup for the panels, buttons, labels
     * etc within the mainmenu.
     **/
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        fileLoadPanel = new javax.swing.JPanel();
        fileLoadedLabel = new javax.swing.JLabel();
        fileNameLabel = new javax.swing.JLabel();
        noRowsLabel = new javax.swing.JLabel();
        loadFileButton = new javax.swing.JButton();
        noRowsDisplay = new javax.swing.JLabel();
        filePanelLabel = new javax.swing.JLabel();
        sensorPanel = new javax.swing.JPanel();
        sensorSelect = new javax.swing.JComboBox<>();
        sensorsLoadedLabel = new javax.swing.JLabel();
        sensorsLoadedNumber = new javax.swing.JLabel();
        transmitterLastSeenLabel = new javax.swing.JLabel();
        transmitterLastSeen = new javax.swing.JLabel();
        transmitterTitleLabel = new javax.swing.JLabel();
        faultsLabel = new javax.swing.JLabel();
        faults = new javax.swing.JLabel();
        latestTransmissionLabel = new javax.swing.JLabel();
        latestTransmission = new javax.swing.JLabel();
        viewErrorsButton = new javax.swing.JButton();
        infoPanel = new javax.swing.JPanel();
        informationPanelTitle = new javax.swing.JLabel();
        receiverUsed = new javax.swing.JLabel();
        receiverUsedLabel = new javax.swing.JLabel();
        systemVersion = new javax.swing.JLabel();
        systemVersionLabel = new javax.swing.JLabel();
        statusCodeLabel = new javax.swing.JLabel();
        statusCode = new javax.swing.JLabel();
        deviceType = new javax.swing.JLabel();
        deviceTypeLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        sensorComboBox = new javax.swing.JComboBox<>();
        sensorTitleLabel = new javax.swing.JLabel();
        sensorReading = new javax.swing.JLabel();
        sensorReadingLabel = new javax.swing.JLabel();
        meanReading = new javax.swing.JLabel();
        meanReadingLabel = new javax.swing.JLabel();
        rangeReadingLabel = new javax.swing.JLabel();
        rangeReading = new javax.swing.JLabel();
        maxValueLabel = new javax.swing.JLabel();
        maxValue = new javax.swing.JLabel();
        minValue = new javax.swing.JLabel();
        minValueLabel = new javax.swing.JLabel();
        displayGraph = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(new java.awt.Dimension(800, 600));

        mainPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        mainPanel.setSize(new java.awt.Dimension(800, 600));

        titleLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        titleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLabel.setText("Environmental Sensors");
        titleLabel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        titleLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        titleLabel.setName("titleLabel"); // NOI18N

        fileLoadPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        fileLoadPanel.setMaximumSize(new java.awt.Dimension(320, 170));
        fileLoadPanel.setMinimumSize(new java.awt.Dimension(320, 170));
        fileLoadPanel.setPreferredSize(new java.awt.Dimension(320, 170));
        fileLoadPanel.setSize(new java.awt.Dimension(320, 180));

        fileLoadedLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        fileLoadedLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        fileLoadedLabel.setText("File Loaded :");

        fileNameLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        fileNameLabel.setText("No file loaded");

        noRowsLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        noRowsLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        noRowsLabel.setText("Number of Rows :");

        loadFileButton.setText("Load File");
        loadFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadFileButtonActionPerformed(evt);
            }
        });

        noRowsDisplay.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        noRowsDisplay.setText("N/A");

        filePanelLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        filePanelLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        filePanelLabel.setText("File");
        filePanelLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        filePanelLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        filePanelLabel.setName("titleLabel"); // NOI18N

        javax.swing.GroupLayout fileLoadPanelLayout = new javax.swing.GroupLayout(fileLoadPanel);
        fileLoadPanel.setLayout(fileLoadPanelLayout);
        fileLoadPanelLayout.setHorizontalGroup(
            fileLoadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fileLoadPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(fileLoadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(filePanelLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(fileLoadPanelLayout.createSequentialGroup()
                        .addGroup(fileLoadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(noRowsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fileLoadedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(fileLoadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(fileNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
                            .addComponent(noRowsDisplay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 33, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, fileLoadPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(loadFileButton)))
                .addContainerGap())
        );
        fileLoadPanelLayout.setVerticalGroup(
            fileLoadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, fileLoadPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(filePanelLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(fileLoadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileLoadedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(fileLoadPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(noRowsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(noRowsDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loadFileButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        sensorPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        sensorPanel.setMaximumSize(new java.awt.Dimension(438, 50));
        sensorPanel.setSize(new java.awt.Dimension(438, 50));

        sensorSelect.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        sensorSelect.setEnabled(false);
        sensorSelect.setMaximumSize(new java.awt.Dimension(96, 27));
        sensorSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensorSelectActionPerformed(evt);
            }
        });

        sensorsLoadedLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        sensorsLoadedLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        sensorsLoadedLabel.setText("Sensors loaded :");

        sensorsLoadedNumber.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        sensorsLoadedNumber.setText("0");

        transmitterLastSeenLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        transmitterLastSeenLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        transmitterLastSeenLabel.setText("This transmitter last :");

        transmitterLastSeen.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        transmitterLastSeen.setText("N/A");

        transmitterTitleLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        transmitterTitleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        transmitterTitleLabel.setText("Transmitter");
        transmitterTitleLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        transmitterTitleLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        transmitterTitleLabel.setName("titleLabel"); // NOI18N
        transmitterTitleLabel.setSize(new java.awt.Dimension(71, 26));

        faultsLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        faultsLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        faultsLabel.setText("Faulty Rows Detected :");

        faults.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        faults.setText("0");

        latestTransmissionLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        latestTransmissionLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        latestTransmissionLabel.setText("Latest transmission :");

        latestTransmission.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        latestTransmission.setText("N/A");

        viewErrorsButton.setText("View Errors");
        viewErrorsButton.setEnabled(false);
        viewErrorsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewErrorsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout sensorPanelLayout = new javax.swing.GroupLayout(sensorPanel);
        sensorPanel.setLayout(sensorPanelLayout);
        sensorPanelLayout.setHorizontalGroup(
            sensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sensorPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sensorPanelLayout.createSequentialGroup()
                        .addComponent(sensorSelect, 0, 182, Short.MAX_VALUE)
                        .addGap(43, 43, 43)
                        .addComponent(sensorsLoadedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sensorsLoadedNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sensorPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(viewErrorsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(faultsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(faults, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(sensorPanelLayout.createSequentialGroup()
                        .addComponent(transmitterLastSeenLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(transmitterLastSeen, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sensorPanelLayout.createSequentialGroup()
                        .addComponent(latestTransmissionLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(latestTransmission, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(transmitterTitleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        sensorPanelLayout.setVerticalGroup(
            sensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sensorPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(transmitterTitleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorsLoadedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorsLoadedNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(faultsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(faults, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(viewErrorsButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(latestTransmissionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(latestTransmission, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(transmitterLastSeenLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(transmitterLastSeen, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        infoPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        informationPanelTitle.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        informationPanelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        informationPanelTitle.setText("Latest Information");
        informationPanelTitle.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        informationPanelTitle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        informationPanelTitle.setName("titleLabel"); // NOI18N
        informationPanelTitle.setSize(new java.awt.Dimension(71, 26));

        receiverUsed.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        receiverUsed.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        receiverUsed.setText("0");

        receiverUsedLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        receiverUsedLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        receiverUsedLabel.setText("Receivers used :");

        systemVersion.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        systemVersion.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        systemVersion.setText("0");

        systemVersionLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        systemVersionLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        systemVersionLabel.setText("System Version :");

        statusCodeLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        statusCodeLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        statusCodeLabel.setText("Status Code :");

        statusCode.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        statusCode.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        statusCode.setText("0");

        deviceType.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        deviceType.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        deviceType.setText("0");

        deviceTypeLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        deviceTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        deviceTypeLabel.setText("Device Type :");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        sensorComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        sensorComboBox.setEnabled(false);
        sensorComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensorComboBoxActionPerformed(evt);
            }
        });

        sensorTitleLabel.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        sensorTitleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sensorTitleLabel.setText("Sensor");
        sensorTitleLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        sensorTitleLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sensorTitleLabel.setName("titleLabel"); // NOI18N
        sensorTitleLabel.setSize(new java.awt.Dimension(71, 26));

        sensorReading.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        sensorReading.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        sensorReading.setText("0");

        sensorReadingLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        sensorReadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        sensorReadingLabel.setText("Latest Sensor Reading :");

        meanReading.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        meanReading.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        meanReading.setText("0");

        meanReadingLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        meanReadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        meanReadingLabel.setText("Mean Reading :");

        rangeReadingLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        rangeReadingLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        rangeReadingLabel.setText("Range Reading :");

        rangeReading.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        rangeReading.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        rangeReading.setText("0");

        maxValueLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        maxValueLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        maxValueLabel.setText("Max Value :");

        maxValue.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        maxValue.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        maxValue.setText("0");

        minValue.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        minValue.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        minValue.setText("0");

        minValueLabel.setFont(new java.awt.Font("Lucida Grande", 0, 16)); // NOI18N
        minValueLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        minValueLabel.setText("Min Value :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sensorTitleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(sensorComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(meanReadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sensorReadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sensorReading, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(rangeReadingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                            .addComponent(maxValueLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                            .addComponent(minValueLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
                        .addGap(286, 286, 286))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(meanReading, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rangeReading, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(maxValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(minValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sensorTitleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorReadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorReading, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(meanReading, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(meanReadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rangeReading, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rangeReadingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(maxValue, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(maxValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(minValue, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(minValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(72, 72, 72))
        );

        displayGraph.setText("Display Graph");
        displayGraph.setEnabled(false);
        displayGraph.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                displayGraphActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout infoPanelLayout = new javax.swing.GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(infoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(informationPanelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(infoPanelLayout.createSequentialGroup()
                        .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(infoPanelLayout.createSequentialGroup()
                                .addComponent(deviceTypeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(deviceType, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(infoPanelLayout.createSequentialGroup()
                                .addComponent(receiverUsedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(receiverUsed, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(infoPanelLayout.createSequentialGroup()
                                .addComponent(systemVersionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(systemVersion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(infoPanelLayout.createSequentialGroup()
                                .addComponent(statusCodeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(statusCode, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(134, 134, 134)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(displayGraph, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        infoPanelLayout.setVerticalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, infoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(informationPanelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(infoPanelLayout.createSequentialGroup()
                        .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(receiverUsedLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(receiverUsed, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(systemVersionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(systemVersion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(statusCodeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(statusCode, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(deviceTypeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(deviceType, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(displayGraph)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(fileLoadPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sensorPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(infoPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(titleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fileLoadPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                    .addComponent(sensorPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(infoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 808, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loadFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadFileButtonActionPerformed
        //Swing component for file requests
        JFileChooser source = new JFileChooser();
        
        /** FileNameExtensionFilter prevents user from selecting anything but CSV.
         * Useful to prevent errors.
        **/
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma Seperated Files", "csv");
        source.setFileFilter(filter);
        
        // User can select file. Checks if one was actually selected.
        if(source.showOpenDialog( null ) == JFileChooser.APPROVE_OPTION)
        {
            // Get user selected file
            File selectedFile = source.getSelectedFile();
            
            data = new dataHandler(selectedFile);
            
            data.loadFile();
            
            int rowsLoaded = data.getRowsLoaded();
            
            data.populateDropdown(sensorSelect);
            
            JOptionPane.showMessageDialog(null, rowsLoaded + " rows loaded successfully", "Loaded Successfully", JOptionPane.INFORMATION_MESSAGE);  //Popup if no file is selected. 
            
            fileNameLabel.setText(source.getSelectedFile().getName());
            
            sensorSelect.setEnabled(true);
            
            noRowsDisplay.setText(Integer.toString(rowsLoaded));
            
            sensorsLoadedNumber.setText(Integer.toString(data.getSensorsLoaded()));
            
            faults.setText(Integer.toString(data.getFaults()));
            
        }
        else {
            JOptionPane.showMessageDialog(null, "No File Selected!", "Error", JOptionPane.ERROR_MESSAGE);  //Popup if no file is selected. 
       }
        
    }//GEN-LAST:event_loadFileButtonActionPerformed

    /**
    Used to update the information box on the home screen.
    Gets the various values associated with the receiver.
    **/
    public void updateInformation()
    {
        data.updateInformation(transmitter, receiverUsed, systemVersion, statusCode, deviceType, latestTransmission);
    }
    
    /**
    Used to update the last seen label whenever the drop down box is changed.
    **/
    public void updateLastSeen()
    {
        String lastSeen = "";
        
        transmitter = String.valueOf(sensorSelect.getSelectedItem());
        
        lastSeen = data.findTransmitter(transmitter);
        
        transmitterLastSeen.setText(lastSeen);
    }
    
    /**
    
    **/
    public void updateSensorBox()
    {
        int sensorNumber = Integer.valueOf((String)sensorComboBox.getSelectedItem());
        data.sensorBoxUpdate(sensorComboBox, transmitter, sensorNumber, sensorReading, meanReading, rangeReading, maxValue, minValue, displayGraph);
    }
    
    /**
    When the transmitter dropdown box is changed, it updates the information
    box and the last seen.
    **/
    private void sensorSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensorSelectActionPerformed
        updateLastSeen();
        updateInformation();
        updateSensorBox();
        viewErrorsButton.setEnabled(true);
    }//GEN-LAST:event_sensorSelectActionPerformed

    /**
    When display graph is clicked it shows the new graph window.
    **/
    private void displayGraphActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_displayGraphActionPerformed
        displayGraph graph = new displayGraph();
        graph.setVisible(true);
        graph.buildGraph(data, String.valueOf(sensorSelect.getSelectedItem()));
    }//GEN-LAST:event_displayGraphActionPerformed

    /**
    When the sensor combo box is changed.
    **/
    private void sensorComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensorComboBoxActionPerformed
        updateSensorBox();
    }//GEN-LAST:event_sensorComboBoxActionPerformed

    private void viewErrorsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewErrorsButtonActionPerformed
       errorFrame errors = new errorFrame();
       errors.setVisible(true);
       System.out.println("RE");
       errors.loadErrors(data);
    }//GEN-LAST:event_viewErrorsButtonActionPerformed

    /***
     * @param args the command line arguments
     **/
    public static void main(String args[]) {
        /** Set the Nimbus look and feel **/
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /** If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         **/
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainmenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /** Create and display the form **/
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new mainmenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel deviceType;
    private javax.swing.JLabel deviceTypeLabel;
    private javax.swing.JButton displayGraph;
    private javax.swing.JLabel faults;
    private javax.swing.JLabel faultsLabel;
    private javax.swing.JPanel fileLoadPanel;
    private javax.swing.JLabel fileLoadedLabel;
    private javax.swing.JLabel fileNameLabel;
    private javax.swing.JLabel filePanelLabel;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JLabel informationPanelTitle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel latestTransmission;
    private javax.swing.JLabel latestTransmissionLabel;
    private javax.swing.JButton loadFileButton;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel maxValue;
    private javax.swing.JLabel maxValueLabel;
    private javax.swing.JLabel meanReading;
    private javax.swing.JLabel meanReadingLabel;
    private javax.swing.JLabel minValue;
    private javax.swing.JLabel minValueLabel;
    private javax.swing.JLabel noRowsDisplay;
    private javax.swing.JLabel noRowsLabel;
    private javax.swing.JLabel rangeReading;
    private javax.swing.JLabel rangeReadingLabel;
    private javax.swing.JLabel receiverUsed;
    private javax.swing.JLabel receiverUsedLabel;
    private javax.swing.JComboBox<String> sensorComboBox;
    private javax.swing.JPanel sensorPanel;
    private javax.swing.JLabel sensorReading;
    private javax.swing.JLabel sensorReadingLabel;
    private javax.swing.JComboBox<String> sensorSelect;
    private javax.swing.JLabel sensorTitleLabel;
    private javax.swing.JLabel sensorsLoadedLabel;
    private javax.swing.JLabel sensorsLoadedNumber;
    private javax.swing.JLabel statusCode;
    private javax.swing.JLabel statusCodeLabel;
    private javax.swing.JLabel systemVersion;
    private javax.swing.JLabel systemVersionLabel;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JLabel transmitterLastSeen;
    private javax.swing.JLabel transmitterLastSeenLabel;
    private javax.swing.JLabel transmitterTitleLabel;
    private javax.swing.JButton viewErrorsButton;
    // End of variables declaration//GEN-END:variables
}
